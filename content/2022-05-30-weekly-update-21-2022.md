+++
title = "Weekly #LinuxPhone Update (21/2022): More work on the PinePhone Pro camera, including a test app by megi plus KDE Eco and GSoC news"
date = "2022-05-30T21:17:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Camera","postmarketOS","eInk","Nemo Mobile",]
categories = ["weekly update"]
authors = ["peter"]
+++

Crickets, tumbleweeds, ... just listen to the new episode of the postmarketOS podcast! Oh, and there are good news regarding Kupfer!
<!-- more -->
_Commentary in italics._

### Hardware that might be interesting
* CNX Software: [$150 InkPalm Plus eReader features RK3566 SoC, 5.84-inch display](https://www.cnx-software.com/2022/05/25/150-inkpalm-plus-ereader-features-rk3566-soc-5-84-inch-display/). _Likely this is just my unhealthy obsession with epaper devices, but assuming the bootloader is somehow unlockable, this might be an interesting little gadget!_

### Software progress
#### Hardware enablement
* megi's PinePhone Development Log: [Pinephone Pro camera pipeline testing app](http://xnux.eu/log/#069). _If I were at home, I would have tested this by now - it all sounds quite promising. I hope the source code of [the app](https://megous.com/git/ppp-cam/tree/) is going to be released soon!_
* megi's PinePhone Development Log: [Pinephone Pro camera improvements](http://xnux.eu/log/#068).

#### GNOME ecosystem
* This Week in GNOME: [#45 Timeout!](https://thisweek.gnome.org/posts/2022/05/twig-45/). _Not much on the mobile front, but it's nice to see Workbench progress!_
* chergert: [Builder GTK 4 Porting, Part V](https://blogs.gnome.org/chergert/2022/05/28/builder-gtk-4-porting-part-v/).
* chergert: [Builder GTK 4 Porting, Part IV](https://blogs.gnome.org/chergert/2022/05/14/builder-gtk-4-porting-part-iv/).
* Danigm: [GNOME Outreachy 2022](https://danigm.net/outreachy-2022.html). _Translations matter!_
* halting problem: [Amberol](https://www.bassi.io/articles/2022/05/25/amberol/).
* Aman: [Beginning my GSoC'22 journey with GNOME](https://www.amankrx.com/blog/gnome-gsoc-intro). _Looking forward to Google-funded non-Google Sync options for GNOME Health!_


#### Plasma/Maui ecosystem
* Nate Graham: [This week in KDE: Resizable Plasma panel pop-ups](https://pointieststick.com/2022/05/27/this-week-in-kde-resizable-plasma-panel-pop-ups/).
* KDAB: [KDE Frameworks - Part 3](https://www.kdab.com/kde-frameworks-part-3/)
* KDE.news: [KDE does Google Summer of Code 2022](https://dot.kde.org/2022/05/26/kde-does-google-summer-code-2022). _Spaces for Neochat, nice!_
* TSDgeos' blog: [Akademy 2022 Call for Participation is open](https://tsdgeos.blogspot.com/2022/05/akademy-2022-call-for-participation-is.html).
* KDE Eco: [Energy Consumption Lab Is Ready For FOSS Community! Dispatch From The Sprint](https://eco.kde.org/blog/2022-05-30-sprint-lab-setup/). _Saving power is good for mobile, so let's have this post and two more reports:_
* Volker Krause: [KDE Eco Sprint May 2022](https://www.volkerkrause.eu/2022/05/28/kde-eco-sprint-may-2022.html).
* Nico's blog: [KDE Eco Sprint 2022](https://nicolasfella.de/posts/eco-sprint/). 

#### Ubuntu Touch
* UBports: [UBports Training Update 4 aka 0100: How to import Python libraries in your Clickable app](https://ubports.com/blog/ubports-news-1/post/ubports-training-update-4-aka-0100-how-to-import-python-libraries-in-your-clickable-app-3847).

#### Nemo Mobile
* Nemo Mobile: [Nemomobile in May 2022](https://nemomobile.net/pages/nemomobile-in-may-2022/).

#### Distro news
* Kupfer Blog: [<code>reboot()</code> - The Future Of Kupfer](https://kupfer.prwn.eu/blog/2022-05-26_future_of_kupfer.html). _There's also a nice new page explaining [Kupferbootstrap](https://kupfer.gitlab.io/kupferbootstrap/) now!_

#### Wayland
* Phoronix: [Wayland 1.21 Alpha Finally Introduces High-Resolution Scroll Wheel Support](https://www.phoronix.com/scan.php?page=news_item&px=Wayland-1.21-Alpha)

#### Kernel
* Phoronix: [Linux 5.19 ARM Excites With Apple M1 NVMe, 12 Year Old Multi-Platform Achievement](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.19-ARM-Changes).
* Phoronix: [MediaTek Vcodec Driver Adds Stateless VP8/VP9 Support In Linux 5.19](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.19-Media-Updates).

### Worth noting
* [HazardChem/PinePhone_Keyboard: Configs and settings for the PinePhone Keyboard - PinePhone_Keyboard - Codeberg.org](https://codeberg.org/HazardChem/PinePhone_Keyboard). _This seems to be a helpful project! I don't have my PinePhone Keyboard at hand (and won't for a while), so I can't really comment on how well things work, but still!_
* [PinePhone Pro Lapdock Arch/Plasma Working - r/pinephone](https://libredd.it/r/pinephone/comments/uwg747/pinephone_pro_lapdock_archplasma_working/). _For those who care about convergence, this is likely lovely news!_

### Worth reading
#### PinePhone Pro and other Cameras
* TuxPhones: [First camera samples from the PinePhone Pro revealed](https://tuxphones.com/pinephone-pro-sony-imx258-camera-demo-samples-mainline-linux/).

#### Ricing Sxmo + Sway
* Porky of the Pine: [How to Rice Your SWMO](https://porkyofthepine.org/blog/rice_sxmo_sway.html). _In case you want to rice your Sway-based Sxmo!_

#### Librem 5 reporting
* janvlug.org: [One week Librem 5 field trial – day 5 to 29](http://janvlug.org/blog/one-week-librem-5-field-trial-day-4-to-29/).

#### Call for feedback
* Purism: [Introducing AweSIM, Simple Plus and SIMple Plans for Securing Your Phone Data](https://puri.sm/posts/introducing-awesim-simple-plus-and-simple-plans-for-securing-your-phone-data/). _I've only included this because I must know if that pricing is somewhat competitive in the US, and how bad privacy violations on other US operators are - please tell me about it! (Context: I am paying 10 Euro for 10 GB/months + unlimited calls and SMS, and Germany is considered a relatively expensive territory compared to other European Countries when it comes to mobile service pricing.)_ 

#### RISC-V or a DevTerm review
* Talospace: [Mini-review: The Clockwork Pi DevTerm R-01, or RISC-V on the go](https://www.talospace.com/2022/05/mini-review-clockwork-pi-devterm-r-01.html). _Well done! In case someone wonders: While running DanctNIX Phosh in a convergent setting, my PinePhone (AllWinner A64) scored 3769.08 iterations/second._

### Worth listening
* postmarketOS podcast: [#18 Biktor's Story, v22.06, SDM845, MSM8916, Bluetooth HFP](https://cast.postmarketos.org/episode/18-Biktors-Story-v2206-SDM845-MSM8916-Bluetooth-HFP/). _Great episode!_

### Worth watching

#### Fun handhelds
* mutantC: [Sony Vaio UX and mutantC V4 comparison](https://www.youtube.com/watch?v=xdUsMp4UtGo)
    
#### Nemo Mobile
* Сергей Чуплыгин: [fixed orientation](https://www.youtube.com/watch?v=d3k8IY38vzw).
* Сергей Чуплыгин: [glacier-gallery simple image edition](https://www.youtube.com/watch?v=Gfa6iV_efsA).
* Jozef Mlich: [Header indicator](https://www.youtube.com/watch?v=a7zepysStJ18).

#### Sailfish OS
* zülkarneyn kaf: [Sailfish OS Sony Xperia 10 III Android App](https://www.youtube.com/watch?v=t0RRCygpi0A). _If you were wondering how well that Android Compatibility layer works: It's pretty good!_

#### Wayland and Plasma 
* KDE Community: [KDE Goals interview: Wayland with Méven Car](https://tube.kockatoo.org/w/q3zbum6VXYiTmi1EbMh2CU)

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

