+++
title = "Weekly GNU-like Mobile Linux Update (17/2023): GloDroid 2.0, Snapshot and a Maui Shell progress video"
date = "2023-05-01T15:28:38Z"
draft = false
[taxonomies]
tags = ["Ubuntu Touch", "Kupfer","Sailfish OS","Maui Shell","Flatseal","Camera","GloDroid","Nemo Mobile",]
categories = ["weekly update"]
authors = ["Peter"]
+++

Also: Updates to Tangram and Flatseal, new Ubuntu Touch 20.04 for the OnePlus One and PinePhone (Pro), swipy input on Phosh OSK Stub, a NemoMobile report, Kupferbootstrap progress, and more!

<!-- more -->
_Commentary in italics._

### Software

#### Gnome Ecosystem
- This Week in GNOME: [#93 Snapshot](https://thisweek.gnome.org/posts/2023/04/twig-93/)
- tchx84: [Flatseal 2.0](https://blogs.gnome.org/tchx84/2023/04/28/flatseal-2-0/)
- Sébastien Wilmet: [The life of a GUI application](https://informatique-libre.be/swilmet/blog/2023-04-25-the-life-of-a-GUI-application.html)
- Felipe Borges: [Thanks everyone for Linux App Summit 2023!](https://feborg.es/thanks-everyone-for-las-2023/)- Planet GNOME: [structure and interpretation of nativescript](https://wingolog.org/archives/2023/04/24/structure-and-interpretation-of-nativescript)
- Guido Günther: [Ever since doing the initial completion support in #phosh &apos;s osk-stub I wanted to add a generic &quot;unix pipe&quot; like completer. phosh-osk-stub would feed the program preedit on stdin and read the completions from the program&apos;s stdout.That is there now and with that and some more changes I can use @zachdecook &apos;s #swipeGuess to do swipe like typing. To be clear: all the hard work is done by swipeGuess, phosh-osk-stub just piggy backs on it:#LinuxMobile #gtk #gnome  #librem5](https://social.librem.one/@agx/110260534404795348)
- [Hometown - Jamie: "very happy to have the first preview release of Snapshot, our new GTK4 camera app…"](https://crab.garden/@jamie/110276935306950627)
- [Sonny: "Tangram 3 is out 🎈 https://f…" - FLOSS.social](https://floss.social/@sonny/110282275399832820)
- [Chris: "Thank you to @bragefuglseth an…" - Fosstodon](https://fosstodon.org/@kop316/110275998407691106)
- [TheEvilSkeleton :silverblue:: "#Flatseal 2.0 was released wit…" - Fosstodon](https://fosstodon.org/@TheEvilSkeleton/110272498598581021)


#### Plasma Ecosystem
- Nate Graham: [This week in KDE: The bug slaughterfest continues](https://pointieststick.com/2023/04/28/this-week-in-kde-the-bug-slaughterfest-continues/)
- Volker Krause: [Linux App Summit 2023](https://www.volkerkrause.eu/2023/04/29/las-2023-push-notifications.html)
- Carl Schwan: [Health of the KDE community (2023 Update)](https://carlschwan.eu/2023/04/28/health-of-the-kde-community-2023-update/)
- ~redstrate: [My work in KDE for April 2023](https://redstrate.com/blog/2023/04/my-work-in-kde-for-april-2023/)
- KDE Eco: [Report From The German Parliament's "Sustainable by Design" Conference](https://eco.kde.org/blog/2023-04-26-sustainable-by-design/)
- Qt blog: [Qbs 2.0 released](https://www.qt.io/blog/qbs-2.0-released)

#### Maui Project
- Maui: [Maui Shell progress video. https://youtu.be/l5hGOriZRak](https://floss.social/@mauiproject/110282426712429755)

#### Ubuntu Touch
- [v0.6.1 · Tags · Oren Klopfer / PinePhone + PinePhone Pro UBTouch Image Builder · GitLab](https://gitlab.com/ook37/pinephone-pro-debos/-/tags/v0.6.1)
- Ubuntu Touch Forums News: [Bacon goes Focal](https://forums.ubports.com/topic/8936/bacon-goes-focal). _That's the OnePlus One from 2014._

#### Sailfish OS
- [Adam Pigg on Twitter: "A fully updated @thepine64 #PinePhonePro with #SailfishOS now has (from the last few days) fixed incoming call and system sounds, fixed microphone volume, LEDs, improved firmware loading." / Twitter](https://twitter.com/adampigg/status/1652738892091596801)
- [carlosgonz: Elisa on SailfishOS](https://mastodon.social/@carlosgonz/110284040638172526). _Keysmith and Angelfish are also available on Chum._

#### Nemo Mobile
- Jozef Mlích: [Nemomobile in April/2023](https://blog.mlich.cz/2023/04/nemomobile-in-april-2023/)
- @jmlich@fosstodon.org: [I had lightning talk about #Nemomobile at @linuxappsummit https://www.youtube.com/live/J7-3Qj_oVMM?feature=share&amp;t=24804](https://fosstodon.org/@jmlich/110252289426718606)

#### Apps
- Purism forums: List of Apps that fit and function well: [FreetuxTV](https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/326)

#### Distributions
- Kupfer blog: [v0.2.0-rc2: Repo Configs and voice calls!](https://kupfer.gitlab.io/blog/2023-04-25_v0-20-rc2_repo-conf.html) _Kupfer is a Arch Linux ARM distribution for Snapdragon 845 and 410/412 devices._
- Nitrux: [Release Announcement: Nitrux 2.8.0 “tf”](https://nxos.org/changelog/release-announcement-nitrux-2-8-0/) _tf for tablet friendly!_
- Manjaro PinePhone Plasma Mobile: [Beta 15 RC2](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta15-rc2)
- Manjaro PinePhone Plasma Mobile: [Beta 15 RC1](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta15-rc1)
- Manjaro PinePhone Phosh: [Beta 31](https://github.com/manjaro-pinephone/phosh/releases/tag/beta31)
- Phoronix: [Debian 12.0 "Bookworm" Planned For Release On 10 June](https://www.phoronix.com/news/Debian-12.0-Release-Date)

#### Glodroid
- glodroid_manifest (GitHub): [GloDroid 2.0](https://github.com/GloDroid/glodroid_manifest/releases/tag/v2.0) _This looks promising, if you find the time to try it, please share your impressions!_

#### Kernel
- phone-devel: [Re: [PATCH 4/4] ARM: dts: qcom: msm8974-hammerhead: Add vibrator](http://lore.kernel.org/phone-devel/918e1b13-c274-a318-9049-a1c72bf12af0@linaro.org/)
- phone-devel: [[PATCH v3 6/8] power: supply: rt5033_charger: Add cable detection and USB OTG supply](http://lore.kernel.org/phone-devel/f66914a2b46651bf7ab9fb2728d13aed460b245c.1682636929.git.jahau@rocketmail.com/)

#### Stack
- Phoronix: [sudo & su Being Rewritten In Rust For Memory Safety](https://www.phoronix.com/news/sudo-su-rewrite-rust)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-04-28](https://matrix.org/blog/2023/04/28/this-week-in-matrix-2023-04-28)

### Past events
- @linmob@fosstodon.org: [Preparing a special treat for tomorrow #LinuxInfoTag2023 Come by if you can! https://linmob.net/see-you-in-augsburg-lit-2023/](https://fosstodon.org/@linmob/110277152352339384)
- @linmob@fosstodon.org: [Finally almost home from #lit2023 -the stand was a blast! Huge thanks to Ollie and cahfofpai for doing this with me, and to everybody who came by!](https://fosstodon.org/@linmob/110284268482651279)
- @linmob@fosstodon.org: [Find our #LIT2023 flyer on https://pinephone.de/ and https://stand.linmob.net/ :)](https://fosstodon.org/@linmob/110287006386581070) _The stand was really something, I hope to write up a post with my imperessions later this week. In case you understand German, you may find [this podcast episode](https://techniktechnik.de/?podcast=tt173-augsburger-linux-infotag-2023) interesting._

### Worth Noting
- PinePhone (Reddit): [Users thoughts on the state of the Pinephone Pro](https://www.reddit.com/r/pinephone/comments/132dczh/users_thoughts_on_the_state_of_the_pinephone_pro/)
- PinePhone (Reddit): [One-handed setup with Pinephone + Twiddler + ReadyAction wrist mount](https://www.reddit.com/r/pinephone/comments/1341yh0/onehanded_setup_with_pinephone_twiddler/)
- PINE64official (Reddit): [What do you use your PineTab for? What are your plans for using a PineTab2?](https://www.reddit.com/r/PINE64official/comments/1316974/what_do_you_use_your_pinetab_for_what_are_your/)
- PINE64official (Reddit): [Is the pine phone usable as a daily driver now?](https://www.reddit.com/r/PINE64official/comments/130by45/is_the_pine_phone_usable_as_a_daily_driver_now/)
- Phones (Librem 5),- Purism community: [Alternative system softwares (keyboard, shell, OS, etc) on Librem 5](https://forums.puri.sm/t/alternative-system-softwares-keyboard-shell-os-etc-on-librem-5/20093)
- @calebccff@fosstodon.org: [did you know pmbootstrap has a fully agnostic flashing interface? it supports fastboot, quirks for separate kernel/ramdisk partitions, heimdall, 0xffffffff and can do adb sideloadall using the same deviceinfo hardware abstraction that makes it possible to ship kernel updates via the package repository just like any other distro](https://fosstodon.org/@calebccff/110287877067737193)
- @calebccff@fosstodon.org: [adding a new &quot;flash_all&quot; function to #pmbootstrap flasher, as well as the ability to pass in arbitrary additional fastboot commands. this will simplify installation slightly as you'll be able to do:pmbootstrap flasher flash_all erase dtbo rebootto flash the kernel &amp; rootfs, erase the dtbo partition and reboot into #postmarketOS](https://fosstodon.org/@calebccff/110287865873513137)
- @dos@librem.one: [Croatia through Librem 5&apos;s eye - photos taken on automatic settings, with interactive comparison between images straight out of Millipixels and those processed in darktable afterwards. #shotonlibrem5 #mobile #gnu #linux #librem5 #photography #camera #croatiahttps://dosowisko.net/l5/photos/](https://social.librem.one/@dos/110284025925962315)
- PINE64official (Reddit): [PINE64: Weekly discussion | April 28, 2023](https://www.reddit.com/r/PINE64official/comments/131p1iq/pine64_weekly_discussion_april_28_2023/)

### Worth Reading
- Purism: [How PureOS Can Stop Devices From Being Infected With Intrusive Adware!](https://puri.sm/posts/how-pureos-can-stop-devices-from-being-infected-with-intrusive-adware/). _Oof._
- David Hamp-Gonsalves: [Junk Drawer Phone as a Music Streaming Server](https://davidhampgonsalves.com/junk-drawer-phone-as-a-music-streaming-server/) _This is nice, despite Termux being the solution._
  - Kevin Purdy: [Old smartphones should be usable as single-board computers, just as this one is | Ars Technica](https://arstechnica.com/gadgets/2023/04/old-phones-can-be-single-board-computers-as-this-mini-music-server-proves/)
  - [Hacker News Comment thread](https://news.ycombinator.com/item?id=35747379)
- Martijn Braam: [NitroKey disappoints me](https://blog.brixit.nl/nitrokey-dissapoints-me/)


### Worth Watching
- Camilo Higuita: [Maui Shell - April 2023 Progress](https://www.youtube.com/watch?v=l5hGOriZRak)
- SVZ-Channel: [Xiaomi Mi A2 Lite running Mobian](https://www.youtube.com/watch?v=KSxzdVlikNY)
- NOT A FBI Honeypot: [Pinephone With FULL disk encryption!! #shorts](https://www.youtube.com/watch?v=0NgB3GoDsMs)
- Marc Raiser: [Librem 5 Linux smartphone from Purism](https://www.youtube.com/watch?v=TaosC1Nek-c)
- twinntech: [Pinephone with a SIM card hows it going](https://www.youtube.com/watch?v=3lXBu-pgskg) _Nice cat, the rest.. I bet I demoed PinePhone phone calls before, more than once. Just saying._

### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

