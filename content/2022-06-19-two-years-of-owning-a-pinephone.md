+++
title = "Two Years of Life with PinePhone"
date = "2022-06-19T12:10:00Z"
updated = "2022-06-20T17:05:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","Community Editions","LinuxPhoneApps","PineTalk",]
categories = ["impressions"]
authors = ["peter"]
[extra]
update_note = "Added some emphasis, clarified here and there, added links to HN, reddit, and other threads. <a href='https://framagit.org/linmob/linmob.frama.io/-/commit/4e176743bfad49b133817bb9f5014ddd961b1944'>Click here to review all edits</a>."
+++

On June 9th, 2020, early into a still raging pandemic, I received my PinePhone UBports Community Edition.[^1] It's incredible how fast these two years went by, and how much the PinePhone experience improved in the meantime. _This is just a subjective post about what I remember, you may also consider it a "two year LINMOB.net revival blog post"!_
<!-- more -->
### Early impressions and doubt

I really liked the PinePhone from the moment I unboxed it, as [I wrote on this blog](https://linmob.net/early-impressions-pinephone-hardware/). What I didn't write as much about was the enormous doubt that set in after a honeymoon period of excitement due to finally being able to try Phosh, LuneOS, Plasma Mobile and Maemo Leste: Battery life __was__ bad. I remember thinking: I need to eat my hat and admit that this might never be working - five to seven hours of idle battery life surely would not be enough for anybody. It would not matter that PinePhone looked like a regular low end to midrange smartphone, that the interfaces were (while unfinished) definitely promising with such a meager battery life. Heck, the Droid 4 I had did way better on a smaller battery, but then it had a SoC that had actually been designed for phones, unlike the AllWinner A64 paired with an equally power-hungry Quectel EG25-G modem.

Fortunately, [CRUST](https://github.com/crust-firmware/crust) became a thing, and while it definitely brought some new challenges (will the phone ever wake up fast enough on phone calls being just one of them) with it, it gave new hope: With the additional caveat of a lack of notifications, something I as a "my phone is in silent mode, no it does not even vibrate, I mean it when I say silent" always knew I could live with, PinePhone could now be a phone for everyday use - for those who where brave enough and independent of proprietary services.

In the meantime, the postmarketOS Community Edition had brought the Convergence Edition to life, delivering equal amounts of RAM and storage (3GB/32GB) as the more expensive Librem 5 at just 50 USD more than the original PinePhone while also including a convergence dock. More importantly, the design bug[^2] that had hindered USB OTG and DisplayPort Alt mode to work had been fixed, enough to convince me to buy a second PinePhone. I wanted to live that old full computer-in-my-pocket dream - that should have had already been working with the UBports CE I had, but did not. Also I just loved the irony of getting a phone pre-installed with postmarketOS (featuring Phosh).

### Slowly maturing: Hardware v1.2b, Community Editions

In that time I would spend a lot of time showing off distributions on video, a hobby that I would only later phase out due to working on other projects: [PineTalk (Season 1)](https://www.pine64.org/pinetalk/) and [LinuxPhoneApps.org](https://linuxphoneapps.org). Also, the end of community editions had made things a bit boring. While Manjaro had made a last minute switch to Phosh, making Purism's UI stack even more of a PinePhone default, the KDE Community Edition shipped Plasma Mobile on a Manjaro base.  I remember attempting to bring the factory image up-to-date around the date devices shipped and ending up with it in a state where at least the GUI wouldn't come up after all. I ended up making a video on the topic of correctly upgrading the Plasma Mobile Community Edition. IIRC, things went fine. Still: Overall it was a bit early for Plasma Mobile in my humble opinion. I don't know if the Community Edition helped with attracting contributors here – but I know that Plasma Mobile is really good these days, so it might have had a positive outcome! 

The last community edition faced a similar "Beware of the initial upgrade" story, but this time I had not expected it: [Mobian](https://www.mobian.org) (also shipping Phosh) had been rock solid in general.[^3] In the time between the creation of the factory image and the device arriving in the hands of customers, a [PAM](https://en.m.wikipedia.org/wiki/Linux_PAM) upgrade had happened, which upended to potentially breaking things when just upgrading without [having read Mobian's blog post beforehand](https://blog.mobian.org/posts/2021/02/09/pam_issue/).

That aside, many Community Editions had few devices shipped with the wrong, previous software, and I think I do recall a handful Q&A issues here and there.

But: It put devices in peoples hand, got the attention of specific groups of distribution afficionados and stabilized the Community projects involved financially, while also putting some strain due to increased support demands on them. 

### The Beta Edition and improvements throughout 2021

Eventually, in March 2021, PINE64 communicated that there would be no further Community Editions. I don't know the back story, whether further approached projects had declined or not. Future PinePhones would ship with the distribution that had shipped with the KDE CE, featuring Plasma Mobile on a Manjaro base, and they would call it the "Beta Edition" to indicate Software status. 

I remember chatting with [Lukasz](https://mobile.twitter.com/LukaszErecinsk1) about this (due to my involvement with PineTalk we did chat rather frequently back then), and the reasoning made sense: 
Manjaro had developers on board for this effort, PINE64 had good relations to KDE e.V., and shipping the competitors Phosh UI on a not community-branded phone would have been problematic. At the same time I felt that Plasma Mobile was not yet in a state on the slow-ish PinePhone that everybody would call "Beta" (especially considering people that would associate "Beta" with pre-release stages of new releases of otherwise mature software). Plasma Mobile did look nice, but it was crashy here and there. Also, my experience with Manjaro had been rather mixed: While it ran fast compared to other distributions - in 2020 they would clock RAM so high that it would be really fast on PinePhone's of lucky owners (including me) and incredibly crashy on less lucky buyers. I had often had bad luck with upgrading installs though - I had ended up with a broken install after having attempted to update an older install after a while, not once, but multiple times. By the end of 2020 I was, due to these experiences, pretty set on never using Manjaro as my daily driver. And boy, can I hold a grudge!

_Additional clarification, June 20th, 2022, ~5 p.m. UTC:_ To be clear, I have kept the default Manjaro Plasma Mobile install on my PinePhone Pro since January, and updating went smoothly so far, no breakages to report. The updating issues of Manjaro thus can be considered solved.


The good thing was that I would not have to use any default install, and all the other distributions would still just be a microSD[HC|XC] card or a Jumpdrive install a way. But I definitely worried about the first impressions of PinePhone people would have, and maybe [rightfully](https://www.youtube.com/watch?v=CBOcCWimdFU) [so](https://www.youtube.com/results?sp=mAEA&search_query=pinephone+twinntech).[^4]

But that goes too far into general history. What were my experiences? Well, I would just use DanctNIX Phosh, experiment with Biktorgj's modem firmware, played with Sailfish OS for a while and also with Sxmo,[^5] took photos with Megapixels and tried to help out people from time to time.[^6] I was excited, when [hardware-accelerated h264 video playback](https://www.youtube.com/watch?v=C4RC3Miuo2A) started working in hacky ways. There were multiple other small little details like this, e.g. when the Night Light setting in Phosh started working, which (fun fact!) still does not work on the Purism Librem 5.

### 2022 and public perception

Recently, when I wrote my weekly update on the PinePhone in a converged setting, I realized how stable things have become, when it comes to Phosh and the software stack below it. Writing a Weekly Update is a time consuming thing that takes me multiple hours, and I recall how hard it was to make use of the Dock at all and how buggy things were when I tried to do so for multiple videos in late 2020. Back then, writing an update in gedit, running Firefox with ~20 tabs for hours would have been plainly impossible without multiple reboots and likely some unfortunate data loss in the meantime. Now, it works effortlessly. Sure, PinePhone is not fast, so you need to relax in your chair to enjoy the process, but: It works!

Since January, I have the PinePhone's faster step brother, too, the PinePhone Pro, a device I have not even written a "first impressions" post about. The same applies to the PinePhone Accessories PINE64 introduced in late 2021. The reason is simply a lack of time: I have a new job, which has increased my overall mental load since Q4 2021, and I finally wanted to launch [LinuxPhoneApps.org](https://linuxphoneapps.org), a process I still do not consider fully finished. Due to this, a lot of stuff has fallen by the wayside, many projects are catching dust in my flat, and my to do list is so long, that I considered making my list of Linux Phone specific To Dos a public one.[^7] There are so many issues to file on various code hosting platforms, so many blog posts to be written, so many wiki entries to be augmented and overhauled!

That said, the accessories seem to be plagued by hardware bugs,[^8] and the PinePhone Pro is, from what I can gather, still not at a point where I would recommend it over the PinePhone. I hope it gets there. 

You may think that it will surely get there, but interest and general excitement seem to have gone down: The number of posts on Social Media (I don't monitor Facebook properties, so this only refers to the Fediverse and Twitter) has gone down, less people create video content about the PinePhone (Pro), so much so that I got back into making videos. This is especially sad, as I think many people would be somewhat pleased if they tried a current distribution on a PinePhone that has beeen catching dust or living in a drawer, despite it not being new and shiny any more.

### Conclusions

Would I get a PinePhone again? Yes, totally. Granted, I have been lucky so far: My hardware has held up well, when I dropped my PinePhones I was lucky enough to not break them, I am not affected by a failing WiFi/BT chip (an issue not too uncommon by my anecdata). No, I don't use it as a daily driver, which was something I thought I would do by this point - but that's on me, not on the PinePhone: I just can't stop buying more hardware, be it Linux Phones and competing platforms. 

I know, some people will wonder if they should buy a PinePhone after reading this, and I can't really answer this question for you. Read up on PINE64's return policy, maybe; check whether your current phone network is compatible and decide whether you could live with the [available applications](https://linuxphoneapps.org/apps/). Watch some videos and decide if you can live with the time Firefox takes to launch and a lack of notifications.[^9] Make sure to have the willingness and time to tinker, to get the deeper "Linux knowledge" necessary if you don't have it, to make whatever distribution you start with your own: This is a far more involved process than upgrading from one iPhone to another, in most cases even more involved than switching between the major platforms! With all this considered, it can be a fun ride, a great learning experience, a nice nerdy thing to do! Oh, and if you do so, make sure to be supportive to the software ecosystem, in contributions and/or funding :-) _Thanks!_

### Call for feedback and questions!

I plan to make a video on the same topic - but I need some more questions/suggestions to be able to make a good video, since I can't possibly make a video about _everything_ that happened in these two years of "Life with PinePhone" - please [send some in](mailto:2years@linmob.net?subject=My%20question%2Fsuggestion%20for%20your%20%22Two%20Years%20with%20PinePhone%20video%22)!


_BTW: I've written this post on my PinePhone :)_

### Addendum
June 20th, 2022, ~ 5 pm UTC: 
This post is being discussed on [hacker news](https://news.ycombinator.com/item?id=31806639), [r/mobilelinux](https://old.reddit.com/r/mobilelinux/comments/vg4nno/two_years_of_life_with_pinephone/), [r/pine64](https://old.reddit.com/r/pine64/comments/vgf2lq/two_years_of_life_with_pinephone/), [r/pinephone](https://old.reddit.com/r/pinephone/comments/vgf2fg/two_years_of_life_with_pinephone/) and [lemmy.ml](https://lemmy.ml/post/326432). 


[^1]: I had ordered a Braveheart PinePhone before this, but it arrived during lock-down and with German Zoll often tending to send the device back due to it not having a CE certification, I did not collect that device and asked customs to send it back, where it seemingly never arrived – I decided to just not care and move on.

[^2]: See [this page on the PINE64 for details](https://wiki.pine64.org/wiki/PinePhone_v1.2a), [the suggested CC fix](https://www.pine64.org/2020/07/15/july-updatepmos-ce-pre-orders-and-new-pinephone-version/) cooperation for pre-1.2a PinePhones became a "discount for a new mainboard" instead. The following Manjaro Community Edition [fixed one more hardware bug](https://wiki.pine64.org/wiki/PinePhone_v1.2b), since then the design has [remained unchanged](https://wiki.pine64.org/wiki/PinePhone#Hardware_revisions), except for the magnetometer, where the original component had become unobtanium due to component shortages. I personally would have liked to see further revisions of the hardware, to fix remaining bugs and [enable faster eMMC speeds](https://izzo.pro/pinephone-vccq-mod/), but it seems unlikely that PINE64 is willing to invest in another PinePhone hardware revision at this point.

[^3]: My only gripe back then was that Mobian felt a lot slower than Danct12's Arch Linux ARM port with Phosh. This is a lot better these days with Mobian being based on "Bookworm".

[^4]: Dear readers, I recognize that this is [confirmation bias](https://en.m.wikipedia.org/wiki/Confirmation_bias) at work!

[^5]: A surprisingly large number of people who daily drive PinePhone do so with Sxmo, which is also the one true native PinePhone experience in a way, since it was started on and for PinePhone: Phosh/GNOME on Mobile is being created for the Purism Librem 5, Plasma Mobile first ran on a LG Nexus 5, Ubuntu Touch made it firsts steps on the Samsung Galaxy Nexus and later the LG Nexus 4, SailfishOS and Nemo Mobile on the Nokia N9/N950 and Maemo Leste was obviously created for the Nokia N900.

[^6]: I still try to do that every then and now. What I have given up on is informing people on Twitter who are discussing Linux Phones and are not well informed (in my opinion). It's just not worth it. 

[^7]: I did not get around to it yet, LOL. Please get in touch if you would be interested to help with getting things done, as I can't justify the effort of making that list public if it does not bring the volume of tasks down. 

[^8]: Examples: Read [dsimic's section on the PinePhone Keyboard accessory in the most recent PINE64 Community Update](https://www.pine64.org/2022/05/31/may-update-worth-the-wait/), the [Wireless Charging case is no longer on sale and use is discouraged](https://wiki.pine64.org/wiki/PinePhone_(Pro)_Add-ons#Qi_Wireless_Charging_Add-on), and there have been reports of the LoRa back case draining the PinePhone battery with the PinePhone switched off. The finger print reader seems to be fine, [except for software support](https://wiki.pine64.org/wiki/PinePhone_(Pro)_Add-ons#Fingerprint_Reader_Add-on).

[^9]: If not, I'd recommend [this device](https://tilvids.com/w/3mn337s4Mx2WPSHRX3KJsz) or the [SHIFT6mq](https://wiki.postmarketos.org/wiki/SHIFT_SHIFT6mq_(shift-axolotl)) right now.
