+++
title = "Weekly GNU-like Mobile Linux Update (44/2022): RISCy Experiments"
date = "2022-11-06T22:30:00Z"
draft = false
[taxonomies]
tags = ["GNOME Shell on Mobile","RISC-V","Maemo Leste","Sailfish OS","Librem 5"]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " with help by Niko and friendly assistance from plata's awesome script"
+++

Also: A bug in VVMd, text prediction for phosh-osk-stub, and Maemo Leste has phones for you! 
<!-- more -->

_Commentary in italics._

### Hardware
- TuxPhones.com: [Homebrew "rvPhone" aims to be the first RISC-V mobile experiment](https://tuxphones.com/risc-v-phone-rvphone-imx8-esp32-sim7600x-e310-nlnet-majstor/) _Interesting... a low-power, low-performance 32-bit RISC-V chip as main chip, plus a iMX8M as a secondary chip might be fun way to achieve battery life and ARM64 Linux when needed._ 

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#68 New Dialogs](https://thisweek.gnome.org/posts/2022/11/twig-68/). _"Money" seems interesting!_
- Niko: [Gnome Shell on Mobile with PostmarketOS + Oneplus 6](https://nikodunk.com/2022-11-05-gnome-shell-mobile-with-postmarketos)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: Big brain KRunner](https://pointieststick.com/2022/11/04/this-week-in-kde-big-brain-krunner/)
- KDE Announcements: [KDE Gear 22.08.3](https://kde.org/announcements/gear/22.08.3/)
- TSDGeos: [KDE Gear 22.12 branches created](https://tsdgeos.blogspot.com/2022/11/kde-gear-2212-branches-created.html)
- Kevin Ottens: [News from KDE PIM in September-October 2022](https://ervin.ipsquad.net/blog/2022/11/02/news-from-kde-pim-in-september-october-2022/)
- Dragotin: [Learnings from Building an AppImage](https://dragotin.wordpress.com/2022/11/01/learnings-from-building-an-appimage/)

#### Sailfish OS
- [Sailfish Community News, 3rd November, Panels and Presentations](https://forum.sailfishos.org/t/sailfish-community-news-3rd-november-panels-and-presentations/13389)

#### Maemo Leste
[Maemo Leste (@maemoleste): "What those create #maemo leste folks mean when they say they have hardware available for developers. These devices just in. Mostly Droid 4, with some Droid 2 and Atrix and Razr devices mixed in."](https://twitter.com/maemoleste/status/1588120000220450816#m) _Not having a device is apparently no longer an excuse to not contributing! ;-)_

#### Distributions
- [Adrian Campos Garrido (@hadrianweb): "Hi all, new improvements in @openSUSE for @thepine64 #Pinephone - Kernel 5.18 (better integration) - U-boot 2022.10 (faster boot time) - Desktop improvements for Phosh and Plasma Mobile https://en.opensuse.org/HCL:PinePhone](https://twitter.com/hadrianweb/status/1588547504831836160#m)

#### Stack 
- Phoronix: [Panfrost Gallium3D Driver Wires Up Mesa Shader Disk Cache Support](https://www.phoronix.com/news/Panfrost-Disk-Cache)
- Phoronix: [Freedreno Gallium3D Now Allows OpenGL 4.5 For Adreno 600 Series GPUs](https://www.phoronix.com/news/Freedreno-A600-OpenGL-4.5)

#### Matrix
- Matrix.org: [This Week in Matrix 2022-11-04](https://matrix.org/blog/2022/11/04/this-week-in-matrix-2022-11-04)

### Worth noting
- krimskrams on Purism community: [Do we have space for more librem 5 reports/reviews?](https://forums.puri.sm/t/do-we-have-space-for-more-librem-5-reports-reviews/18634). _scrcpy is a great way to remotely run a few Android apps!_
- "[Chris Talbot] released vvmd 0.12 https://gitlab.com/kop316/vvmd/-/tags/0.12 which contains the fix to [this bug](https://fosstodon.org/@kop316/109277618755660744). is still required to update the settings if you are on T-mobile [USA] (mainly, disable then reenable VVM). It probably isn't a bad idea if you are using it period to do this when you use 0.12 just to be safe." [Original toot](https://fosstodon.org/@kop316/109279702922916526)
- [Guido Günther: "I'm really good at making typos so I revisited the text-completion support in #phosh's osk-stub and added a completer based on the #presage library and trained it with a German text from  @gutenberg_org and was surprised how well that works"](https://social.librem.one/@agx/109286561751410903). _Truly impressive!_

### Worth reading
- Michael Cantanzaro: [Stop Using QtWebKit](https://blogs.gnome.org/mcatanzaro/2022/11/04/stop-using-qtwebkit/)
- Javier Martinez Canillas: [How to install Fedora on an HP X2 Chromebook](https://blog.dowhile0.org/2022/11/04/how-to-install-fedora-on-an-hp-x2-chromebook/)
- dcz: [Graphical debugging on the Librem 5 using QtCreator](https://dcz_self.gitlab.io/posts/remote_debugging/)
- Purism: [Cheers, to the Future of Libcamera](https://puri.sm/posts/cheers-to-the-future-of-libcamera/)

### Worth watching
- Niko: [Linux on smartphones in 2022 - Mobile Gnome Shell + postmarketOS on a OnePlus 6](https://www.youtube.com/watch?v=wOmRMg546UY)
- Continuum Gaming: [Microsoft Continuum Gaming E337: Sailfish OS – A couple of additional games](https://www.youtube.com/watch?v=JMUhjysa42g)
- LiveG Technologies: [LiveG Lookahead · Episode 2](https://www.youtube.com/watch?v=CaF17Hj6jEA?t=903)

### Thanks

Huge thanks to Niko for contributing to this installment of the Weekly Update and also to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email - or just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

PPS: This one has (once again) less headlines - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update%2039&body=Less%20or%20more%20headlines%20going%20forward%3F)

