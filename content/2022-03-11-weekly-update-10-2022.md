+++
title = "Weekly Update (10/2022): Squeekboard gets a new look, Nheko 0.9.2, Dirty Pipes and Tow-Boot being released for PinePhone and PinePhone Pro"
date = "2022-03-11T22:05:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","Librem 5","Convergence","Tow-Boot","Squeekboard","Manjaro","f(x)tec Pro1","Nheko","UBports", ]
categories = ["weekly update"]
authors = ["peter"]
+++

There's some good news and some bad news. Dirty Pipe is not a fun vulnerability, and our GNU/Linux Phones have new enough kernels to be vulnerable. At least that new Spectre variant does not affect Cortex A53 phones - having in-order cores only is not always disadvantageous.
<!-- more -->

_Commentary in italics._

### Hardware enablement
* Danct12 has ported postmarketOS to the original [Snapdragon 835 variant of the Fxtec Pro1](https://fosstodon.org/web/@danct12/107913613391915684).

### Software news

#### Security
* ArsTechnica: [Dirty Pipe - Linux has been bitten by its most high-severity vulnerability in years](https://arstechnica.com/information-technology/2022/03/linux-has-been-bitten-by-its-most-high-severity-vulnerability-in-years/).

#### Firmware
* [Tow-Boot 2021.10-004: Now more mobile](https://github.com/Tow-Boot/Tow-Boot/releases/tag/release-2021.10-004) has been released, supporting the PinePhone and PinePhone Pro among many other ARM devices. _The great thing about Tow-Boot is that it supports [EBBR](https://arm-software.github.io/ebbr/index.html#document-chapter2-uefi), which basically means Unified Extensible Firmware Interface (UEFI) and thus standardized booting on your phone._


#### GNOME ecosystem
* [Squeekboard 1.17 has been released](https://gitlab.gnome.org/World/Phosh/squeekboard/-/merge_requests/531), featuring an updated visual design among other things.
* Marcus Lundblad: [Maps and GNOME 42](https://ml4711.blogspot.com/2022/03/maps-and-gnome-42.html).
* GNOME: [GSoC 2022: GNOME Foundation has been accepted as a mentor organization!](https://feborg.es/gsoc-2022-gnome-accepted/).
* This Week in GNOME: [#34 No Backup No Mercy](https://thisweek.gnome.org/posts/2022/03/twig-34/).


#### Plasma/Maui ecosystem
* KDE has been [accepted into GSoC](https://twitter.com/kdecommunity/status/1501165761498427394), [these are potential projects](https://community.kde.org/GSoC/2022/Ideas).
* KDE Eco: [Celebrate Energy Conservation Day With KDE Eco](https://eco.kde.org/blog/2022-03-05-energy-conservation-day/). _Preserving energy also has benefits for resource constrained mobile devices. ;-)_
* Tsdgeos: [Okular: Signature support now works on Android](https://tsdgeos.blogspot.com/2022/03/okular-signature-support-now-works-on.html).

#### Other app releases
* Nheko (Matrix client) [have released 0.9.2](https://fosstodon.org/@deepbluev7/107929186750640828). _Message bubbles!_

#### Distro releases
* [Manjaro ARM with Plasma Mobile Beta 11](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta11) has been released.

#### Sailfish OS
* flypig: [Sailfish Community News, 10 March, MWC Barcelona](https://forum.sailfishos.org/t/sailfish-community-news-10-march-mwc-barcelona/10607/1)

#### UBports
* UBports news: [Your biweekly UBports news during troubled times!](https://ubports.com/de/blog/ubports-blogs-nachrichten-1/post/your-biweekly-ubports-news-during-troubled-times-3838). _Ubuntu Touch OTA 22, JingPad images and more._

### Worth noting
* If you like minimal setups, [you'll like what kelbot came up with #PineDA](https://retro.social/@kelbot/107929432214057801.)

### Worth reading
#### Tow-Boot
* TuxPhones: [Booting ARM Linux the standard way](https://tuxphones.com/booting-arm-linux-the-standard-way/).
* Phoronix: [Tow-Boot Sees New Release As User-Friendly U-Boot Distribution](https://www.phoronix.com/scan.php?page=news_item&px=Tow-Boot-2021.10-004).

#### Developer Tools
* Janet Blackquill: [New tools in the QML LSP collection: qml-dap, qml-dbg, and qml-lint](https://blog.blackquill.cc/new-tools-in-the-qml-lsp-collection-qml-dap-qml-dbg-and-qml-lint).

#### Convergence
* Kyle Rankin for Purism: [My First Year of Librem 5 Convergence](https://puri.sm/posts/my-first-year-of-librem-5-convergence/). _Nice report! I wish Purism would enable zram by default._


### Worth listening

* PineTalk: [Episode 6: The Community has been ACTIVE!](https://www.pine64.org/2022/03/06/s02e06-pinetalk-the-community-has-been-active/)

### Potentially Worth watching[^1]
#### PinePhone Pro Initial Impressions
* Jacob David Cunningham: [Pinephone Pro Explorer Edition first couple of days](https://www.youtube.com/watch?v=oUS52Lfekdo).
* Bitter Epic Skits: [PinePhone Pro Explorer Edition UNBOXING. I finally have it!](https://www.youtube.com/watch?v=9g-ma7lTIc0).
* Canadian Bitcoiners: [PinePhone Pro Unboxing - The Ultimate Privacy Phone Is Here?](https://www.youtube.com/watch?v=4aA5atAgzmM). _Just a word regarding the title: It's a phone meant to run Linux, that's still in the process of hardware enablement. Yes, GNU/Linux is less spyware ridden than other operating systems, but does this make the PinePhone Pro a "privacy smartphone"? You can probably make it one, but we're not there yet._

#### Security Tutorial
* (RTP) Privacy Tech Tips: [🔒 Lynis Scanner: Linux Hardening](https://www.youtube.com/watch?v=jMGYtgPvwYI)

#### Ubuntu Touch
* CyberPunked: [Ubuntu Touch Installation - OnePlus 3 - Android 9 Downgrade](https://www.youtube.com/watch?v=b4WkjUkHUzw).


### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

[^1]: Sadly I did not have time to watch any of the longer videos in here this week. So... have fun!
