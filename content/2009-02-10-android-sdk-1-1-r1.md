+++
title = "Android SDK 1.1 R1"
aliases = ["2009/02/10/android-sdk-1-1-r1"]
author = "peter"
date = "2009-02-10T00:25:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "SDK", "T-Mobile G1"]
categories = ["hardware", "software"]
authors = ["peter"]
+++
As RC33 (which is the first US Android 1.1 release) for T-Mobile G1 was released recently for the US G1s and as the new G1s for continental europe have already been released with firmware 1.1, it was time for a new SDK.

Main improvement is a better german localization support and several bugfixes.

Check out the <a href="http://developer.android.com/sdk/android-1.1.html">release notes</a> and grab the <a href="http://developer.android.com/sdk/1.1_r1/index.html">__S__oftware__D__evelopment__K__it</a>

_BTW: I have been thinking recently about weekly a linux mobile roundup to make this blog more attractive. Guess I´ll do that from next saturday on.. so watch out!_
