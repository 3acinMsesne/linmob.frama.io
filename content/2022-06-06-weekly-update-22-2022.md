+++
title = "Weekly #LinuxPhone Update (22/2022): A new, quite different Linux Phone and GNOME Shell for Mobile"
date = "2022-06-06T14:55:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Mobile GNOME Shell","Linux on iPhone","Linux on iPad","Sailfish OS","Phosh","Phoc","Notkia",]
categories = ["weekly update"]
authors = ["peter"]
+++

Also: Multiple Librem 5 usage reports/impressions/reviews, a glimpse at Sailfish OS on the Sony XPERIA 10 III, booting Linux on older iOS devices, and I'm sick with COVID19.

<!-- more -->
_Commentary in italics._

### Top Story: GNOME Shell on Mobile
A major bomb of an announcement dropped only minutes after the last Weekly Update: 

* [Towards GNOME Shell on mobile – GNOME Shell & Mutter](https://blogs.gnome.org/shell-dev/2022/05/30/towards-gnome-shell-on-mobile/)

Six weeks ago, [we featured a brief note about public funding for GNOME Shell Mobile](https://linmob.net/weekly-update-16-2022/#worth-noting). Back then, I thought that this was likely about the ecosystem or Phosh, and did not click the Code link on the [funding page](https://prototypefund.de/en/project/gnome-shell-mobile/) - time is scarce, and thus I missed that this actually was about GNOME Shell. As the blog post and Calebccff's videos ([1](https://twitter.com/calebccff/status/1531737951612411904#m), [2](https://twitter.com/calebccff/status/1531737951612411904#m) show, this is already quite smooth and impressive. Sure, there's a lot of work to be done, e.g. regarding the virtual keyboard.

Now what does this mean for Phosh[^1]? According to Purism Developer [Sebastian Krzyszkowiak](https://teddit.net/r/Purism/comments/v1jwpr/rest_in_piece_phosh/iasjjx2/#c), it does not change anything in the short term, as e.g. Calls UI for accepting can be reused with Phosh easily, while it would require more work with GNOME Shell, since that does not use GTK, but a custom, Clutter-based toolkit. Sounds like GNOME Shell Mobile is going to be more for tablets or PDAs in the short term – which is great, too! If this saddens you, because you're longing for gestures or a different app drawer: Phosh is getting some gestures with 0.20, and more might land soon if open [Merge Requests](https://gitlab.gnome.org/World/Phosh/phosh/-/merge_requests) can be taken as an indicator.

Having already spoiled that stacks are too different to transfer improvements (beyond design work) from Mutter/Shell to wlroots-based Phosh/Phoc or vice versa, and assuming you're afraid of duplicate efforts (as a user): Don't worry about it. Duplicate efforts happen all the time in FOSS land, intentionally or unintentionally. Looking at Qt land and the shells of Plasma Mobile, Maui Shell, CutiePi Shell, Nemo Mobile, Lomiri, CuboCore's shell and whatever the thing JingOS shipped was called again - duplicated effort does not hurt, as long as the people driving the efforts have enough dedication and continued motivation to make them great. So let's be supportive and be glad to have yet another great FOSS option on smaller and larger touch screens!

If you are impatient want to try it, you can do so on [postmarketOS](https://gitlab.com/postmarketOS/pmaports/-/tree/feature/gnome-mobile) or [Manjaro](https://gitlab.manjaro.org/manjaro-arm/packages/community/gnome-mobile).[^2]

### Hardware 
#### Notkia
* [Notkia - Hackster.io](https://www.hackster.io/reimunotmoe/notkia-f6e772). _This is an interesting project: Taking the refined shell of an older dumb phone, and re-using it for your fun project. Note that the term "phone" is better to be used in airquotes: The device does not feature GSM/UMTS/LTE to connect to regular phone networks. LoRa has enough bandwith for texting, but IIRC not for voice calls. The SoC is low power, MIPS32 architecture by Ingenic, which should be powerful enough to allow for VoIP calling - and all software will have to be quite efficient, as 64 Megabytes of RAM are not a lot.[^3] I like it, despite it's likely not for me!_
* [Notkia | Hackaday.io](https://hackaday.io/project/185645-notkia). _Same project, different hosting._
   * [Notkia puts a Linux PC inside the shell of a Nokia 1680 cellphone - Liliputing](https://liliputing.com/2022/06/notkia-puts-a-linux-pc-inside-the-shell-of-a-nokia-1680-cellphone.html). _Brad's take!_

### Software progress

#### GNOME ecosystem
##### (Pre-)Releases
* [Phosh 0.20~beta 1](https://social.librem.one/@agx/108402292329373672) is out, featuring gesture support. _I've [tried it](https://twitter.com/linmobblog/status/1533497801782575104#m) on my Librem using [the Octarine repo](https://source.puri.sm/Librem5/developer.puri.sm/-/issues/193) – I like it a lot!_
+ [Phoc 0.20 was released before that](https://social.librem.one/@dos/108397987638094326), delivering the necessary foundational changes.
* [Squeekboard release 1.18.0 - Librem / Phones (Librem 5) - Purism community](https://forums.puri.sm/t/squeekboard-release-1-18-0/17413)
* [Calls 43 Alpha 1](https://gitlab.gnome.org/GNOME/calls/-/tags/v43.alpha.1) has also been tagged, supporting SRTP for some encryption.

##### News
* This Week in GNOME: [#46 Going Mobile](https://thisweek.gnome.org/posts/2022/06/twig-46/).
* Jonas Dressler for the  GNOME Shell & Mutter blog: [Towards GNOME Shell on mobile](https://blogs.gnome.org/shell-dev/2022/05/30/towards-gnome-shell-on-mobile/). _See above._
* chergert: [Builder GTK 4 Porting, Part VI](https://blogs.gnome.org/chergert/2022/06/06/builder-gtk-4-porting-part-vi/).
* Martín Abente Lahaye: [Flatseal 1.8.0](https://blogs.gnome.org/tchx84/2022/05/31/flatseal-1-8-0/).
* Federico's Blog: [Accessibility repositories are now merged](https://viruta.org/accessibility-repos-are-now-merged.html).
* Phoronix: [GNOME's Mutter Moving Closer To Pure Wayland-Only Build Option - Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=GNOME-Mutter-Prep-X11-Free).

#### Plasma/Maui ecosystem

* Nate Graham: [This week in KDE: Fixing bugs, and let’s fix more](https://pointieststick.com/2022/06/03/this-week-in-kde-fixing-bugs-and-lets-fix-more/).
* Volker Krause: [April/May in KDE Itinerary](https://www.volkerkrause.eu/2022/06/04/kde-itinerary-april-may-2022.html).
* Felipe Kinoshita: [My week in KDE: More Tasks and New App](https://fhek.gitlab.io/en/my-week-in-kde-more-tasks-and-new-app/). _Nice, morse code!_
* Felipe Kinoshita: [My week in KDE: Improvements to Tasks](https://fhek.gitlab.io/en/my-week-in-kde-improvements-to-tasks/).
* KDE e.V.: [KDE e.V. is looking for a developer to help further KDE’s presence on app stores](https://ev.kde.org//2022/06/01/job-project-kde-appstores/).

#### Ubuntu Touch


#### Sailfish OS
* flypig: [Sailfish Community News, 2nd June, VoLTE roundup](https://forum.sailfishos.org/t/sailfish-community-news-2nd-june-volte-roundup/11764).
#### Distro news
* Mobian's Blog: [This month in Mobian: May 2022](https://blog.mobian.org/posts/2022/05/31/tmim/).

#### Non-Linux
* Genode: [Release notes for the Genode OS Framework 22.05](https://genode.org/documentation/release-notes/22.05#PinePhone).

#### Apps
* Mardy: [MiTubo 1.1: screensaver inhibitor](http://www.mardy.it/blog/2022/06/mitubo-11-screensaver-inhibitor.html).


### Worth noting
* [webOS Archive](https://www.webosarchive.com/). _If you're missing webOS, and still have a device in a drawer, have a look!_
* Meanwhile, LuneOS, webOS continuation into the present, [is moving forward into a Qt6 future](https://twitter.com/webosports/status/1530916686517379075)!
* [Do not ship work in progress](https://do-not-ship.it/). _An Open Letter, you may recognize some names._
* [Pi-Apps on Librem 5 with PureOS - Phones (Librem 5) - Purism community](https://forums.puri.sm/t/pi-apps-on-librem-5-with-pureos/17399). _More sources for apps, maybe  (not a recommendation!)..._
* [caleb (@calebccff): "yes this means "untethered" dual booting - you can run android and postmarketOS and switch between them without a computer!"](https://twitter.com/calebccff/status/1533054049200787456#m). _Dual booting Android and Linux on the OnePlus 6 (and other devices), yeah!_

### Worth reading

#### PINE64 Community Update
* PINE64: [May Update: Worth The Wait](https://www.pine64.org/2022/05/31/may-update-worth-the-wait/). _Make sure to read the part about the PinePhone Keyboard if you have one!_

#### Fruity hardware running Linux
* Ars Technica: [Have an old iPad lying around? You might be able to make it run Linux soon](https://arstechnica.com/gadgets/2022/06/developers-get-linux-up-and-running-on-old-ipad-air-2-hardware/). _Go read this, it's a great achievement! Multiple iPhones (5s, 6) and iPads (Air, Air 2, Mini 2-4) use these chips and are all no longer supported by Apple, which is slowly but surely making them into mostly useless machines. With this, some more use cases become likely – and less of them have to go to the landfill for no good reason!_
* Worth Doing Badly: [Hardware-accelerated virtual machines on jailbroken iPhone 12 / iOS 14.1](https://worthdoingbadly.com/hv/).

#### Librem 5 Impressions
* Kevux: [Librem to Freedom](https://kevux.org/news/2022_06_04-librem_to_freedom.html). _You may think that this one is overly positive – make sure to not miss the personal context!_
* Yaal: [Purism Librem 5 software review : our list to Santa](https://yaal.coop/blog/en/purism-librem5-software-santas-list). _Basically a to do list! Great job! Click [here](https://forums.puri.sm/t/my-detailed-ticket-list-to-santa/17439/6) for my nitpicks!_
* [A Humble Librem 5 Daily User Review - Librem / Phones (Librem 5) - Purism community](https://forums.puri.sm/t/a-humble-librem-5-daily-user-review/17435)

##### PinePhone Pro Impressions
* The New Oil: [Daily Driving the Pinephone Pro](https://blog.thenewoil.org/daily-driving-the-pinephone-pro).


#### Firefox and potential future mobile power effiency
* Phoronix: [Firefox Nightly Tries For VA-API Video Acceleration For Mesa Users](https://www.phoronix.com/scan.php?page=news_item&px=Firefox-103-Nightly-VA-API).

#### Off Topic: Happy Birthday, Phoronix!
* Phoronix: [Phoronix Turns 18 Years Old For Covering Linux Hardware](https://www.phoronix.com/scan.php?page=news_item&px=Phoronix-18). _If you have been following and enjoying Phoronix, now is an affordable time to [get more and give back](https://www.phoronix.com/scan.php?page=news_item&px=Phoronix-18-Birthday-Special)!_


### Worth listening
* PineTalk: [S02E08: So Porky Is Now A Developer …](https://www.pine64.org/2022/06/03/6517/).

### Worth watching[^4]
#### PINE64 Community Update
* PINE64: [May Community Update: Worth The Wait](https://www.youtube.com/watch?v=wQaUaP2s130). _Great synopsis by PizzaLovingNerd!_

#### Sailfish OS
* Leszek Lesner: [SailfishOS on the Xperia 10 III - Quick Overview](https://www.youtube.com/watch?v=gFFLYRQtooY).
* Leszek Lesner: [Xperia 10 II vs Xperia 10 III - Running SailfishOS - Which one is better?](https://www.youtube.com/watch?v=h7Hr45d9Ki0).

#### Gaming
* Vilda: [PinePhone - Xonotic gameplay](https://www.youtube.com/watch?v=rXjbyvUmO-w)

#### GNOME Shell on postmarketOS
* Martijn Braam: [Gnome stack performance on the PinePhone and Samsung Galaxy SII](https://www.youtube.com/watch?v=zbPOLPpyGDM).



### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!


[^1]: Phosh was only started because GNOME Shell developers considered a mobile adaption of Shell not viable. To be fair, that was multiple years ago - GNOME Shell performance improved a lot since then, and with available hardware and the wide adoption of libhandy and libadwaita in the GNOME ecosystem circumstances are totally different!

[^2]: I've wanted to test this on the OG PinePhone (it's always interesting to see how well stuff runs on slow hardware) and failed – I blame COVID. Sadly, this my failure means that I won't be able to provide guidance.

[^3]: Even the Openmoko Neo Freerunner featured 128 MB RAM - but given Motorola shipped its EZX Linux Phones with a meager 48MB RAM and a GUI (including VoIP support on the A910), it's a challenging, but not impossible project to make this a useful device.

[^4]: There may be some videos I did not find this week - I'll blame being sick and will include ommitted older videos in next weeks Weekly Update.
